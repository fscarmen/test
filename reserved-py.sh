#!/usr/bin/env bash
read -rp "输入reserved: " RESERVED
if [ "${#RESERVED}" = 4 ]; then
  cat >./temp.py << EOF
import base64
reserved = "$RESERVED"
decode = list(base64.b64decode(reserved))
print("解码后:")
print(decode)
EOF
else
  cat >./temp.py << EOF
import base64
encoded_bytes = base64.b64encode(bytes($RESERVED))
encoded_str = encoded_bytes.decode("utf-8")
print("编码后:")
print(encoded_str)
EOF
fi
python3 temp.py
rm -f temp.py
