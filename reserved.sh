#!/usr/bin/env bash
read -rp "输入reserved: " RESERVED
if [ "${#RESERVED}" = 4 ]; then
  DECODE=$(echo "$RESERVED" | base64 -d | xxd -p | fold -w2 | while read HEX; do printf '%d ' "0x${HEX}"; done | awk '{print "["$1", "$2", "$3"]"}')
  echo "解码后: $DECODE"
else
  BYTE[0]=$(grep -oE '[0-9]+' <<< "$RESERVED" | head -n 1)
  BYTE[1]=$(grep -oE '[0-9]+' <<< "$RESERVED" | sed -n '2p')
  BYTE[2]=$(grep -oE '[0-9]+' <<< "$RESERVED" | tail -n 1)
  ENCODE=$(echo "$RESERVED" | printf '%02x' ${BYTE[0]} ${BYTE[1]} ${BYTE[2]} | xxd -r -p | base64)
  echo "编码后: $ENCODE"
fi
